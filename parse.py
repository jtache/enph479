#!/usr/bin/env python3
'''
Author(s):
    Jan Tache

Description:
    This file contains the code to parse eye gaze tracker data and output a
    normalized pupil size measurement for each time step. Note that this file
    requires python3.

    Non standard lib packages used are:
        scipy, matplotlib, numpy

Notes:
    Seems like the normalizing using glint distances is not very good. Glint
    distances probably vary based on pupil angle in a difficult to predict way,
    so it's not a reliable means of normalizing for face distance from the
    camera.
'''

from typing import List
from functools import reduce
from scipy import interpolate
from scipy.signal import butter,lfilter,filtfilt
from math import isnan,sqrt,floor
from gazedata import *
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import sys
import os

def parseThenPlot(calc: PupilCalc, fname: str, label: str = None) -> \
        (List[matplotlib.lines.Line2D], List[str], float):
    '''
    Higher level logic for parsing the eye gaze data file. Look into the rest
    of the file to see the helper functions and structs (namedtuples).

    Returns (plot handles, plot labels, and t0 = start epoch time
    '''
    # Lists for graphing
    tl, tr, yl, yr = parseFile(fname, calc)

    # Set t=0 to be the first sample
    t0: float = 0.0

    # Code to make sure that garbage data isn't selected
    if tl[0] == -1:
        t0 = tr[0]
    elif tr[0] == -1:
        t0 = tl[0]
    else:
        t0 = min(tl[0], tr[0])

    # Take the average of the first 10 samples to be the baseline and
    # compute everything else to be ratios with the baseline.
    yl_i = reduce(lambda x, y: x + y, yl[0:10]) / 10
    yr_i = reduce(lambda x, y: x + y, yr[0:10]) / 10

    # use relative size instead of absolute (arbitrary units)
    yl_rel = list(map(lambda y: y / yl_i, yl))
    yr_rel = list(map(lambda y: y / yr_i, yr))

    # Now process the data with interpolation and filtering to prepare for
    # plotting
    handles: List[matplotlib.lines.Line2D] = []
    labels: List[str] = []

    plt.figure(0)

    # Plot individual eyes in a try-except in the case that the data is garbage
    try:
        tfiltl, yfiltl = myfilter(tl, yl_rel, t0)
        pleft, = plt.plot(tfiltl, yfiltl)
        handles.append(pleft)

        if label == None:
            labels.append('left')
        else:
            labels.append('left, '+label)
    except:
        print('Could not filter left eye data')

    try:
        tfiltr, yfiltr = myfilter(tr, yr_rel, t0)
        pright, = plt.plot(tfiltr, yfiltr)
        handles.append(pright)

        if label == None:
            labels.append('right')
        else:
            labels.append('right, '+label)
    except:
        print('Could not filter right eye data')

    plt.legend(handles, labels)
    plt.xlabel('t since start (s)')
    plt.ylabel('relative size of pupil from baseline at the beginning')
    plt.title('Relative left and right pupil sizes over time')

    plt.grid(True)
    plt.figure(1)
    plotAverage(tl, yl_rel, tr, yr_rel)

    return handles, labels, t0

# All the helper classes and functions

def parseFile(fname: str, calc: PupilCalc) -> \
        (List[float], List[float], List[float], List[float]):
    '''
    Parse the data file. Returns, in order:
        1. timestamps for valid left eye data
        2. timestamps for valid right eye data
        3. left eye data
        4. right eye data

    If there is 0 valid data for one eye, then its two lists will be of length
    1 with the single entry being -1.
    '''
    with open(fname, 'r') as f:
        # Validate that we're reading the file correctly
        if (not indicesValid(f.readline().split(','))):
            print('Exiting with critical error: an index was invalid!\n')
            os.exit(1)

        left: List[float] = []
        right: List[float] = []
        time_left: List[float] = []
        time_right: List[float] = []

        # get data from the file
        for line in f:
            line_data = line.split(',')

            gaze_data = parseGaze(line_data)
            if gaze_data == None:
                continue

            if not gaze_data.pupil_found_left and \
                    not gaze_data.pupil_found_right:
                continue

            pupil_sizes = computePupilSizes(gaze_data, calc)

            if pupil_sizes.left != 0.0 and not isnan(pupil_sizes.left):
                left.append(pupil_sizes.left)
                time_left.append(gaze_data.timestamp)

            if pupil_sizes.right != 0.0 and not isnan(pupil_sizes.right):
                right.append(pupil_sizes.right)
                time_right.append(gaze_data.timestamp)

    if len(time_left) == 0:
        time_left.append(-1)
        left.append(-1)

    if len(time_right) == 0:
        time_right.append(-1)
        right.append(-1)

    return time_left, time_right, left, right

def myinterp(t: List[float], y: List[float], t0: float) -> \
        (List[float], List[float], float):
    '''
    Returns spline interpolated evenly spaced data:
    (new t values, new y value, even spacing between t values)
    '''
    t = list(map(lambda t: (t-t0)/1e6, t))
    f = interpolate.interp1d(t, y)
    new_t = np.linspace(min(t), max(t), len(t)*3) # arbitrary 3x oversampling
    new_y = f(new_t)
    return new_t, new_y, new_t[1] - new_t[0]

def myfilter(t: List[float], y: List[float], t0: float) -> \
        (List[float], List[float]):
    '''
    Returns filtered data output and the corresponding timestamps. Uses spline
    interpolation to produce regular data points and then low pass filering
    to remove high frequency intrument noise.

    Uses scipy interpolate and scipy.signal butter+filtfilt.

    From what I can tell, filtfilt produces more accurate filtering but it
    cannot be used live (offline, pre-recorded data only).

    Below explanation of filtfilt vs lfilter taken from:
    https://dsp.stackexchange.com/questions/19084/applying-filter-in-scipy-signal-use-lfilter-or-filtfilt

    filtfilt is zero-phase filtering, which doesn't shift the signal as it
    filters. Since the phase is zero at all frequencies, it is also
    linear-phase. Filtering backwards in time requires you to predict the
    future, so it can't be used in "online" real-life applications, only for
    offline processing of recordings of signals.

    lfilter is causal forward-in-time filtering only, similar to a real-life
    electronic filter. It can't be zero-phase. It can be linear-phase
    (symmetrical FIR), but usually isn't. Usually it adds different amounts of
    delay at different frequencies.
    '''
    t, y, spacing = myinterp(t, y, t0)
    b, a = butter(2, spacing)
    return t, filtfilt(b, a, y)

def plotAverage(tl: List[float], yl: List[float], tr: List[float],
        yr: List[float]):
    '''
    Plots the average pupil data for left and right pupils. The input should
    nott be interpolated data - it should use the original timestamps. Any gaps
    in the data for either eye will be filled in with the previous data point.
    '''
    if tl[0] == -1 or tr[0] == -1:
        return

    l_set = set(tl)
    r_set = set(tr)

    # Get unique sorted list of times
    t_all = []
    t_all.extend(tl)
    t_all.extend(list(r_set - l_set))
    t_all.sort()

    # Prepare to compute average for each time value
    l_prev = yl[0]
    r_prev = yr[0]

    l_dict = {}
    for i in range(0,len(tl)):
        l_dict[tl[i]] = yl[i]

    r_dict = {}
    for i in range(0,len(tr)):
        r_dict[tr[i]] = yr[i]

    y_all = []

    # Compute the average for each time value
    for t in t_all:
        l = l_prev
        r = r_prev

        if t in l_dict:
            l = l_dict[t]

        if t in r_dict:
            r = r_dict[t]

        y_all.append((l+r)/2)

        l_prev = l
        r_prev = r

    # Plotting now
    t, y = myfilter(t_all, y_all, t_all[0])
    plt.plot(t, y)

    plt.xlabel('t since start (s)')
    plt.ylabel('relative size of pupil from baseline at the beginning')
    plt.title('Relative average of left+right pupil sizes over time')
    plt.grid(True)

    plt.figure(2)
    plotFirstDerivative(t, y)
    plt.figure(3)
    plotSecondDerivative(t, y)

def plotFirstDerivative(t: List[float], y: List[float]):
    '''
    Plots the first derivative for the given t and y values. The data should
    already be interpolated.

    Uses centered difference to compute the derivative at each point:
        dy[i]/dt = (y[i+1] - y[i-1]) / (2*dt)
    '''
    tdiff = t[0] - t[1]

    y_p = []
    y_p.append(0) # 0 for first value

    for i in range(1, len(y)-1):
        y_p.append((y[i+1] - y[i-1]) / (2*tdiff))

    y_p.append(0) # 0 for last value

    plt.plot(t, y_p)
    plt.xlabel('t since start (s)')
    plt.ylabel('Time derivative of relative averaged pupil sizes')
    plt.title('Time derivative of relative averaged pupil sizes')
    plt.grid(True)

    return None


def plotSecondDerivative(t: List[float], y: List[float]):
    '''
    Plots the second derivative for the given t and y values. The data should
    already be interpolated, ie. even spacing in time values.

    Uses centered difference to compute the second derivative at each point:
        d^2y[i]/dt^2 = (y[i+1] - 2*y[i] + y[i-1]) / (dt^2)
    '''
    tdiff = t[0] - t[1]
    y_p = []
    y_p.append(0) # 0 derivative for first value

    for i in range(1, len(y)-1):
        y_p.append((y[i+1] - 2*y[i] + y[i-1]) / (tdiff*tdiff))

    y_p.append(0) # 0 derivative for last value

    plt.plot(t, y_p)
    plt.xlabel('t since start (s)')
    plt.ylabel('2nd time derivative of relative averaged pupil sizes')
    plt.title('2nd time derivative of relative averaged pupil sizes')
    plt.grid(True)

    return None

# Startup code
if __name__ == '__main__':
    if len(sys.argv) != 2 and len(sys.argv) != 3:
        print('Usage:\npython <scriptname> <data filename> <?occlusion time>')
    else:
        fname = sys.argv[1]

        if 1:
            # Plot and then place the "occlusion" time
            h,l,t0 = parseThenPlot(PupilCalc.Raw, fname)
            if len(sys.argv) == 3:
                occlusion_time = float(sys.argv[2])
                hnew, = plt.plot([occlusion_time-t0/1e6],[1],'x')
                plt.legend(h + [hnew], l+['occlusion time'])
            plt.show()

        if 0:
            # Compare "Raw" vs "Norm" calculation
            h1,t1,t0 = parseThenPlot(PupilCalc.Raw, fname, 'raw')
            h2,t2,_ = parseThenPlot(PupilCalc.Norm, fname, 'norm')
            plt.legend(h1+h2, t1+t2)
            plt.show()
