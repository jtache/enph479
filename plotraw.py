from typing import List,NamedTuple
from math import isnan
from scipy import interpolate
from scipy.signal import butter,filtfilt
import matplotlib.pyplot as plt
import numpy as np
import sys

class GazeData(NamedTuple):
    '''Data from the gaze tracker CSV file'''
    timestamp: float
    pupil_right_width: float
    pupil_right_height: float
    pupil_right_angle: float
    pupil_found_right: bool
    glint0_right_x: float
    glint0_right_y: float
    glint1_right_x: float
    glint1_right_y: float
    glint2_right_x: float
    glint2_right_y: float
    pupil_left_width: float
    pupil_left_height: float
    pupil_left_angle: float
    pupil_found_left: bool
    glint0_left_x: float
    glint0_left_y: float
    glint1_left_x: float
    glint1_left_y: float
    glint2_left_x: float
    glint2_left_y: float

def parseThenPlot(fname: str):
    data = parseFile(fname)

    t: List[float] = []
    y: List[float] = []

    t0 = data[0].timestamp
    t = list(map(lambda y: (y.timestamp - t0)/1e6, data))

    y = list(map(lambda y: y.pupil_left_width, data))
    process_xy(t, y, 'left width')

    y = list(map(lambda y: y.pupil_left_height, data))
    process_xy(t, y, 'left height')

    y = list(map(lambda y: y.pupil_right_width, data))
    process_xy(t, y, 'right width')

    y = list(map(lambda y: y.pupil_right_height, data))
    process_xy(t, y, 'right height')

def process_xy(x: List[float], y: List[float], label: str=None):
    '''Selects non garbage data then plots'''
    x2: List[float] = []
    y2: List[float] = []

    for i in range(0,len(y)):
        if y[i] != 0.0 and not isnan(y[i]):
            y2.append(y[i])
            x2.append(x[i])

    x2, y2 = myfilter(x2, y2, 0)

    plot(x2, y2, label)

def myfilter(t: List[float], y: List[float], t0: float) -> List[float]:
    '''Returns filtered data output'''
    t, y, spacing = myinterp(t, y, t0)
    b, a = butter(2, spacing)
    return t, filtfilt(b, a, y)

def myinterp(t: List[float], y: List[float], t0: float) \
        -> (List[float], List[float], float):
    '''Returns polynomial interpolated, evenly spaced data'''
    t = list(map(lambda t: (t-t0), t))
    f = interpolate.interp1d(t, y)
    new_t = np.linspace(min(t), max(t), len(t)*3) # arbitrary 3x oversampling
    new_y = f(new_t)
    return new_t, new_y, new_t[1] - new_t[0]

def plot(x: List[float], y: List[float], label: str):
    if not label:
        plt.plot(x, y)
    else:
        print(label)
        plt.plot(x, y, label=label)

    plt.xlabel('t since start (s)')
    plt.ylabel('some value')
    plt.grid(True)

def parseFile(fname: str) -> List[GazeData]:
    '''
    Parse the data file. Returns, in order:
        1. timestamps for valid left eye data
        2. timestamps for valid right eye data
        3. left eye data
        4. right eye data
    '''

    data: List[GazeData] = []

    with open(fname, 'r') as f:
        # Validate that we're reading the file correctly
        if (not indicesValid(f.readline().split(','))):
            print('Exiting with critical error: an index was invalid!\n')
            os.exit(1)

        # get data from the file
        for line in f:
            line_data = line.split(',')

            gaze_data = parseGaze(line_data)
            if gaze_data != None:
                data.append(gaze_data)

    return data

def parseGaze(line_data: List[str]) -> GazeData:
    '''Returns a PupilData named tuple using the parameter data.
    The indexes of line_data should exactly match Irene's gaze tracker
    output format.

    The members pupil_found_right and pupil_found_left are type Bool.
    The rest of the tuple members are floating points.'''
    if len(line_data) < 45:
        return None

    try:
        return GazeData(
            float(line_data[0]), float(line_data[14]), float(line_data[15]),
            float(line_data[16]), bool(line_data[17]), float(line_data[18]),
            float(line_data[19]), float(line_data[20]), float(line_data[21]),
            float(line_data[22]), float(line_data[23]), float(line_data[35]),
            float(line_data[36]), float(line_data[37]), bool(line_data[38]),
            float(line_data[39]), float(line_data[40]), float(line_data[41]),
            float(line_data[42]), float(line_data[43]), float(line_data[44]))
    except:
        print('Found unparsable row:', line_data)
        return None

def indicesValid(line_data: List[str]) -> bool:
    '''Input parameter should be from the first line of the CSV for the
    eye gaze tracker. Returns true if the headers match what is expected
    and false otherwise.

    If an error is generated from this function, make sure to modify the
    indices in both this function, as well as the function parseGaze.
    '''
    # Yes, strings are being put into a tuple that should be bools and floats.
    # Python doesn't actually care about this. This is the easiest thing to do
    # for now.
    gd = GazeData(
            line_data[0], line_data[14], line_data[15],
            line_data[16], line_data[17], line_data[18],
            line_data[19], line_data[20], line_data[21],
            line_data[22], line_data[23], line_data[35],
            line_data[36], line_data[37], line_data[38],
            line_data[39], line_data[40], line_data[41],
            line_data[42], line_data[43], line_data[44])

    if gd.timestamp != 'timestamp':
        print('Column index for <timestamp> is incorrect.')
        return False
    if gd.pupil_right_width != 'pupil_right_width':
        print('Column index for <pupil_right_width> is incorrect.')
        return False
    if gd.pupil_right_height != 'pupil_right_height':
        print('Column index for <pupil_right_height> is incorrect.')
        return False
    if gd.pupil_right_angle != 'pupil_right_angle':
        print('Column index for <pupil_right_angle> is incorrect.')
        return False
    if gd.pupil_found_right != 'pupil_found_right':
        print('Column index for <pupil_found_right> is incorrect.')
        return False
    if gd.glint0_right_x != 'glint0_right_x':
        print('Column index for <glint0_right_x> is incorrect.')
        return False
    if gd.glint0_right_y != 'glint0_right_y':
        print('Column index for <glint0_right_y> is incorrect.')
        return False
    if gd.glint1_right_x != 'glint1_right_x':
        print('Column index for <glint1_right_x> is incorrect.')
        return False
    if gd.glint1_right_y != 'glint1_right_y':
        print('Column index for <glint1_right_y> is incorrect.')
        return False
    if gd.glint2_right_x != 'glint2_right_x':
        print('Column index for <glint2_right_x> is incorrect.')
        return False
    if gd.glint2_right_y != 'glint2_right_y':
        print('Column index for <glint2_right_y> is incorrect.')
        return False
    if gd.pupil_left_width != 'pupil_left_width':
        print('Column index for <pupil_left_width> is incorrect.')
        return False
    if gd.pupil_left_height != 'pupil_left_height':
        print('Column index for <pupil_left_height> is incorrect.')
        return False
    if gd.pupil_left_angle != 'pupil_left_angle':
        print('Column index for <pupil_left_angle> is incorrect.')
        return False
    if gd.pupil_found_left != 'pupil_found_left':
        print('Column index for <pupil_found_left> is incorrect.')
        return False
    if gd.glint0_left_x != 'glint0_left_x':
        print('Column index for <glint0_left_x> is incorrect.')
        return False
    if gd.glint0_left_y != 'glint0_left_y':
        print('Column index for <glint0_left_y> is incorrect.')
        return False
    if gd.glint1_left_x != 'glint1_left_x':
        print('Column index for <glint1_left_x> is incorrect.')
        return False
    if gd.glint1_left_y != 'glint1_left_y':
        print('Column index for <glint1_left_y> is incorrect.')
        return False
    if gd.glint2_left_x != 'glint2_left_x':
        print('Column index for <glint2_left_x> is incorrect.')
        return False
    if gd.glint2_left_y != 'glint2_left_y':
        print('Column index for <glint2_left_y> is incorrect.')
        return False
    return True

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage:\npython <scriptname> <data filename> <?occlusion time>')
    else:
        fname = sys.argv[1]
        parseThenPlot(fname)
        plt.legend()
        plt.show()
