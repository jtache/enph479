'''
This file contains enums, named tuples, and functions to deal with the raw
gaze data.
'''

from typing import List,NamedTuple
from enum import Enum

class PupilCalc(Enum):
    '''
    Used to state which pupil size calculation should be used. This is for
    computing the pupil size at a given time. This does not have anything to do
    with filtering - only normalizing.

    To know what each enum value will do, see the function:
        computePupilSizes
    '''
    Raw = 1
    Norm = 2

class GazeData(NamedTuple):
    '''Data from the gaze tracker CSV file for one line'''
    timestamp: float
    pupil_right_width: float
    pupil_right_height: float
    pupil_right_angle: float
    pupil_found_right: float
    glint0_right_x: float
    glint0_right_y: float
    glint1_right_x: float
    glint1_right_y: float
    glint2_right_x: float
    glint2_right_y: float
    pupil_left_width: float
    pupil_left_height: float
    pupil_left_angle: float
    pupil_found_left: float
    glint0_left_x: float
    glint0_left_y: float
    glint1_left_x: float
    glint1_left_y: float
    glint2_left_x: float
    glint2_left_y: float
    pog_right_x: float
    pog_right_y: float
    pog_valid_right: float
    pog_left_x: float
    pog_left_y: float
    pog_valid_left: float

class GlintDistances(NamedTuple):
    '''Computed GlintDistances from a GazeData object'''
    r01: float
    r02: float
    r12: float
    r_has_0: bool
    l01: float
    l02: float
    l12: float
    l_has_0: bool

class PupilSizes(NamedTuple):
    '''left+right pupil size for a given timestamp'''
    left: float
    right: float

class Type(Enum):
    '''
    Not sure how to pass a "type" as a parameter, so using an enum to represent
    types instead. Maybe this is the pythonic way of doing it? The only
    relevant types in the gaze data right now are "Float" and "String".
    '''
    Float = 1
    String = 2

def parseGaze(line_data: List[str]) -> GazeData:
    '''
    Returns a PupilData named tuple using the parameter data.
    The indexes of line_data should exactly match Irene's gaze tracker
    output format.
    '''
    return parseGazeType(line_data, Type.Float)

def parseGazeType(line_data: List[str], data_type: Type) -> GazeData:
    '''
    Parse the gaze data line members into a specified type.
    This implementation of the function is not the most efficient way to do
    this, but it is more maintainable than the more efficient methods. If
    Python had some sort of syntax for branch prediction hints, this could be
    made more efficient.
    '''
    if len(line_data) < 45:
        print('Length of line data is wrong')
        return None

    ts = tryValueType(line_data[0], data_type)

    pog_rx = tryValueType(line_data[3], data_type)
    pog_ry = tryValueType(line_data[4], data_type)
    pog_rv = tryValueType(line_data[5], data_type)

    pup_rw  = tryValueType(line_data[14], data_type)
    pup_rl  = tryValueType(line_data[15], data_type)
    pup_ra  = tryValueType(line_data[16], data_type)
    pup_rf  = tryValueType(line_data[17], data_type)

    glt_0rx = tryValueType(line_data[18], data_type)
    glt_0ry = tryValueType(line_data[19], data_type)
    glt_1rx = tryValueType(line_data[20], data_type)
    glt_1ry = tryValueType(line_data[21], data_type)
    glt_2rx = tryValueType(line_data[22], data_type)
    glt_2ry = tryValueType(line_data[23], data_type)

    pog_lx = tryValueType(line_data[24], data_type)
    pog_ly = tryValueType(line_data[25], data_type)
    pog_lv = tryValueType(line_data[26], data_type)

    pup_lw  = tryValueType(line_data[35], data_type)
    pup_ll  = tryValueType(line_data[36], data_type)
    pup_la  = tryValueType(line_data[37], data_type)
    pup_lf  = tryValueType(line_data[38], data_type)

    glt_0lx = tryValueType(line_data[39], data_type)
    glt_0ly = tryValueType(line_data[40], data_type)
    glt_1lx = tryValueType(line_data[41], data_type)
    glt_1ly = tryValueType(line_data[42], data_type)
    glt_2lx = tryValueType(line_data[43], data_type)
    glt_2ly = tryValueType(line_data[44], data_type)

    return GazeData(
            ts, pup_rw, pup_rl, pup_ra, pup_rf, glt_0rx, glt_0ry, glt_1rx,
            glt_1ry, glt_2rx, glt_2ry, pup_lw, pup_ll, pup_la, pup_lf,
            glt_0lx, glt_0ly, glt_1lx, glt_1ly, glt_2lx, glt_2ly, pog_rx,
            pog_ry, pog_rv, pog_lx, pog_ly, pog_lv)

def tryValueType(val: str, data_type: Type):
    '''
    Parse a value and return it as the given type.
    '''
    if data_type == Type.Float:
        try:
            return float(val)
        except:
            return 0.0
    else:
        return val

def indicesValid(line_data: List[str]) -> bool:
    '''
    Input parameter should be from the first line of the CSV for the
    eye gaze tracker. Returns true if the headers match what is expected
    and false otherwise.

    If false is returned from this function, modify the indices in the function:
        parseGazeType
    '''
    # Yes, strings are being put into a tuple that should be floats. Python
    # doesn't actually care about this. This is the easiest thing to do for
    # now.
    gd = parseGazeType(line_data, Type.String)

    if gd.timestamp != 'timestamp':
        print('Column index for <timestamp> is incorrect.')
        return False
    if gd.pupil_right_width != 'pupil_right_width':
        print('Column index for <pupil_right_width> is incorrect.')
        return False
    if gd.pupil_right_height != 'pupil_right_height':
        print('Column index for <pupil_right_height> is incorrect.')
        return False
    if gd.pupil_right_angle != 'pupil_right_angle':
        print('Column index for <pupil_right_angle> is incorrect.')
        return False
    if gd.pupil_found_right != 'pupil_found_right':
        print('Column index for <pupil_found_right> is incorrect.')
        return False
    if gd.glint0_right_x != 'glint0_right_x':
        print('Column index for <glint0_right_x> is incorrect.')
        return False
    if gd.glint0_right_y != 'glint0_right_y':
        print('Column index for <glint0_right_y> is incorrect.')
        return False
    if gd.glint1_right_x != 'glint1_right_x':
        print('Column index for <glint1_right_x> is incorrect.')
        return False
    if gd.glint1_right_y != 'glint1_right_y':
        print('Column index for <glint1_right_y> is incorrect.')
        return False
    if gd.glint2_right_x != 'glint2_right_x':
        print('Column index for <glint2_right_x> is incorrect.')
        return False
    if gd.glint2_right_y != 'glint2_right_y':
        print('Column index for <glint2_right_y> is incorrect.')
        return False
    if gd.pupil_left_width != 'pupil_left_width':
        print('Column index for <pupil_left_width> is incorrect.')
        return False
    if gd.pupil_left_height != 'pupil_left_height':
        print('Column index for <pupil_left_height> is incorrect.')
        return False
    if gd.pupil_left_angle != 'pupil_left_angle':
        print('Column index for <pupil_left_angle> is incorrect.')
        return False
    if gd.pupil_found_left != 'pupil_found_left':
        print('Column index for <pupil_found_left> is incorrect.')
        return False
    if gd.glint0_left_x != 'glint0_left_x':
        print('Column index for <glint0_left_x> is incorrect.')
        return False
    if gd.glint0_left_y != 'glint0_left_y':
        print('Column index for <glint0_left_y> is incorrect.')
        return False
    if gd.glint1_left_x != 'glint1_left_x':
        print('Column index for <glint1_left_x> is incorrect.')
        return False
    if gd.glint1_left_y != 'glint1_left_y':
        print('Column index for <glint1_left_y> is incorrect.')
        return False
    if gd.glint2_left_x != 'glint2_left_x':
        print('Column index for <glint2_left_x> is incorrect.')
        return False
    if gd.glint2_left_y != 'glint2_left_y':
        print('Column index for <glint2_left_y> is incorrect.')
        return False
    if gd.pog_right_x != 'pog_right_x':
        print('Column index for <pog_right_x> is incorrect.')
        return False
    if gd.pog_right_y != 'pog_right_y':
        print('Column index for <pog_right_y> is incorrect.')
        return False
    if gd.pog_valid_right != 'pog_valid_right':
        print('Column index for <pog_valid_right> is incorrect.')
        return False
    if gd.pog_left_x != 'pog_left_x':
        print('Column index for <pog_left_x> is incorrect.')
        return False
    if gd.pog_left_y != 'pog_left_y':
        print('Column index for <pog_left_y> is incorrect.')
        return False
    if gd.pog_valid_left != 'pog_valid_left':
        print('Column index for <pog_valid_left> is incorrect.')
    return True

def computeGlintDistances(gd: GazeData) -> GlintDistances:
    '''
    Returns a GlintDistances tuple formed from gaze values in param gd.

    The members will all be floating points. This should be formed from
    calculations using a GazeData tuple's members.
    '''
    # Anonymous function to compute distance between the two points
    # (x1,y1) and (x2,y2)
    d = lambda x1,y1,x2,y2: sqrt((x2-x1)**2 + (y2-y1)**2)

    r01 = d(gd.glint0_right_x, gd.glint0_right_y,
            gd.glint1_right_x, gd.glint1_right_y)
    r02 = d(gd.glint0_right_x, gd.glint0_right_y,
            gd.glint2_right_x, gd.glint2_right_y)
    r12 = d(gd.glint1_right_x, gd.glint1_right_y,
            gd.glint2_right_x, gd.glint2_right_y)
    r_has_0 = (gd.glint0_right_x == 0.0 or gd.glint0_right_y == 0.0 or
               gd.glint1_right_x == 0.0 or gd.glint1_right_y == 0.0 or
               gd.glint2_right_x == 0.0 or gd.glint2_right_y == 0.0)

    l01 = d(gd.glint0_left_x, gd.glint0_left_y,
            gd.glint1_left_x, gd.glint1_left_y)
    l02 = d(gd.glint0_left_x, gd.glint0_left_y,
            gd.glint2_left_x, gd.glint2_left_y)
    l12 = d(gd.glint1_left_x, gd.glint1_left_y,
            gd.glint2_left_x, gd.glint2_left_y)
    l_has_0 = (gd.glint0_left_x == 0.0 or gd.glint0_left_y == 0.0 or
               gd.glint1_left_x == 0.0 or gd.glint1_left_y == 0.0 or
               gd.glint2_left_x == 0.0 or gd.glint2_left_y == 0.0)

    return GlintDistances(r01,r02,r12,r_has_0,
                          l01,l02,l12,l_has_0)

def computePupilSizes(gdata: GazeData, calc: PupilCalc) -> PupilSizes:
    '''
    Use GazeData and GlintDistances to compute left and right pupil sizes
    according to the calc parameter.

    For param calc:
    - PupilCalc.Raw will just use the maximum of (pupil width, pupil_height).
    - PupilCalc.Norm will use the same value as Raw but divide it by the
      average distance between glints.
    '''
    l_pupil_dia = max(gdata.pupil_left_width, gdata.pupil_left_height)
    r_pupil_dia = max(gdata.pupil_right_width, gdata.pupil_right_height)

    if calc == PupilCalc.Raw:
        return PupilSizes(l_pupil_dia, r_pupil_dia)

    elif calc == PupilCalc.Norm:
        gdist = computeGlintDistances(gdata)

        left = 0.0
        right = 0.0

        left_avg_dists = ((gdist.l01 + gdist.l02 + gdist.l12) / 3)
        if left_avg_dists != 0:
            left = l_pupil_dia / left_avg_dists

        right_avg_dists = ((gdist.r01 + gdist.r02 + gdist.r12) / 3)
        if right_avg_dists != 0:
            right = r_pupil_dia / right_avg_dists

        if gdist.l_has_0:
            left = 0.0

        if gdist.r_has_0:
            right = 0.0

        return PupilSizes(left, right)
    else:
        return None
